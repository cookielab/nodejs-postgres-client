# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).
This change log adheres to standards from [Keep a CHANGELOG](http://keepachangelog.com).

## [Unreleased]

## [7.0.0]
### Removed
- `toSQL` support removed. Use custom types.

## [6.0.0] 2020-11-24
### Added
- Use `@cookielab.io/nodejs-backend-scripts` package

### Changed
- Update dependencies
- Use `prepack` script instead of `prepare`

### Removed
- Remove support for Flow type
- Drop support for Node.js < 12.13.0

## [5.0.0] 2020-07-02
### Added
- support for Node.js 12
- column query methods
    - `getColumn`
    - `findOneColumn`
    - `getOneColumn`
- `BatchDeleteCollector`
- `DatabaseDeleteStream`
- added `CHANGELOG.md`

### Changed
- update dependencies
- improved documentation in `README.md`
- refactored `BatchInsertCollector`
- add generic parameters to some classes and methods to improve typing experience
- refactored validation of unfinished streams (in transactions)
- refactored transaction callback execution
- rewritten checking of conflicting queries in transactions
- enforce correct ending of transaction callbacks
    - especially for stream usages
- other under the hood improvements, simplifications and fixes

### Removed
- support for Node.js older than `10.13.0`
- return value from `insert` method

### Fixed
- dependency on the same way created object for values table transformer
    - should not fail for more cases
    - might fail in DB instead of in code for some logic error (e.g. same amount of keys in multiple objects but different names)
- missing `readonly` modifier
- fix possibly unreleased client on query stream creation
- fix database error stack trace in inner transactions using debug mode

## [4.1.0] 2019-08-29
### Changed
- update dependencies

## [4.0.0] 2019-06-24
### Changed
- update dependencies
- make all types more strict
    - use class property modifiers `public`, `protected` and `private`
        - results in a few new getters to be used instead of direct property access
    - use `readonly` as much as possible

### Removed
- not needed private property `itemsCount` from `DatabaseInsertStream`

### Fixed
- a few tests have used wrong results in mock implementations

## [3.0.1] 2019-01-29
### Changed
- update dependencies

### Fixed
- fix concurrency of inner transactions to keep queries in savepoints where expected

## [3.0.0] 2019-01-14
### Changed
- migrated from Flow to Typescript
- update dependencies

### Removed
- support for Node.js older than `8.10.0`
