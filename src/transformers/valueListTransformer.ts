import sqlFragmentMapper from './sqlFragmentMapper';
import {SQL, SqlFragment} from '../SQL';

type BaseType = Date | boolean | number | string | null;
type RowValueType = BaseType | BaseType[];

const valueListTransformer = <T extends RowValueType>(values: readonly T[]): SqlFragment => {
	if (values.length < 1) {
		throw new Error('Cannot create list of values from empty array.');
	}

	return sqlFragmentMapper(
		values,
		(value: T) => SQL`${value}`,
		', '
	);
};

export default valueListTransformer;
