import sqlFragmentMapper from './sqlFragmentMapper';
import {SQL, SqlFragment} from '../SQL';

const columnNamesTransformer = (columns: readonly string[]): SqlFragment => {
	if (columns.length < 1) {
		throw new Error('Cannot create list of column names from empty array.');
	}

	return sqlFragmentMapper(
		columns,
		(column: string) => SQL`$columnName${column}`,
		', '
	);
};

export default columnNamesTransformer;
