import {SQL, SqlFragment} from '../SQL';

const columnNameTransformer = (column: string): SqlFragment => {
	return SQL`$identifier${column}`;
};

export default columnNameTransformer;
