import {SQL, SqlFragment} from '../SQL';
import {Row} from '../Row';

const insertTransformer = <T extends Row>(row: T): SqlFragment => {
	return SQL`$multiInsert${[row]}`;
};

export default insertTransformer;
