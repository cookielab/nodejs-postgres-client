import BatchDeleteCollector from './collectors/BatchDeleteCollector';
import BatchInsertCollector from './collectors/BatchInsertCollector';
import Client from './Client';
import DatabaseDeleteStream from './streams/DatabaseDeleteStream';
import DatabaseInsertStream from './streams/DatabaseInsertStream';
import DatabaseReadStream from './streams/DatabaseReadStream';
import OneRowExpectedError from './errors/OneRowExpectedError';
import TypeNotFoundError from './errors/TypeNotFoundError';
import assignmentTransformer from './transformers/assignmentTransformer';
import columnNameTransformer from './transformers/columnNameTransformer';
import columnNamesTransformer from './transformers/columnNamesTransformer';
import insertTransformer from './transformers/insertTransformer';
import isUniqueViolation from './isUniqueViolation';
import multiInsertTransformer from './transformers/multiInsertTransformer';
import valueListTransformer from './transformers/valueListTransformer';
import valuesTableTransformer from './transformers/valuesTableTransformer';
import {SQL, registerTransform, SqlFragment, removeTransform} from './SQL';

registerTransform('columnName', columnNameTransformer);
registerTransform('columnNames', columnNamesTransformer);
registerTransform('values', valueListTransformer);
registerTransform('valuesTable', valuesTableTransformer);
registerTransform('assign', assignmentTransformer);
registerTransform('multiInsert', multiInsertTransformer);

removeTransform('insert_object');
registerTransform('insert_object', insertTransformer);

removeTransform('insert');
registerTransform('insert', insertTransformer);

export type {Connection} from './Connection';
export type {Row} from './Row';

// eslint-disable-next-line import/no-unused-modules
export {
	BatchDeleteCollector,
	BatchInsertCollector,
	Client,
	DatabaseDeleteStream,
	DatabaseInsertStream,
	DatabaseReadStream,
	isUniqueViolation,
	OneRowExpectedError,
	SQL,
	SqlFragment,
	TypeNotFoundError,
};
