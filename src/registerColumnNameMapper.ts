import columnNameTransformer from './transformers/columnNameTransformer';
import {registerTransform, removeTransform} from './SQL';

export type ColumnNameMapper = (column: string) => string;

const registerColumnNameMapper = (mapper: ColumnNameMapper): void => {
	removeTransform('columnName');
	registerTransform('columnName', (column: string) => columnNameTransformer(mapper(column)));
};

export default registerColumnNameMapper;
