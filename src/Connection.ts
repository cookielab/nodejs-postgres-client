import {QueryConfig, QueryResult} from 'pg';

import {DeleteCollectorOptions, OneDatabaseValue} from './collectors/BatchDeleteCollector';
import {InsertCollectorOptions} from './collectors/BatchInsertCollector';
import {Row} from './Row';
import DatabaseDeleteStream from './streams/DatabaseDeleteStream';
import DatabaseInsertStream from './streams/DatabaseInsertStream';
import DatabaseReadStream from './streams/DatabaseReadStream';

export interface AsyncQueryable {
	readonly query: <T extends Row = Row>(input: QueryConfig | string, values?: readonly any[]) => Promise<QueryResult<T>>; // eslint-disable-line @typescript-eslint/no-explicit-any
}

export interface AsyncConnection extends AsyncQueryable {
	readonly findOne: <T extends Row = Row>(input: QueryConfig | string, values?: readonly any[]) => Promise<T | null>; // eslint-disable-line @typescript-eslint/no-explicit-any
	readonly findOneColumn: <T>(input: QueryConfig | string, values?: readonly any[], columnIndex?: number) => Promise<T | null>; // eslint-disable-line @typescript-eslint/no-explicit-any
	readonly getOne: <T extends Row = Row>(input: QueryConfig, error: {new(...parameters: readonly any[]): Error}) => Promise<T>; // eslint-disable-line @typescript-eslint/no-explicit-any
	readonly insert: <T extends Row = Row>(table: string, values: T) => Promise<void>;

	readonly getRow: <T extends Row = Row>(input: QueryConfig | string, values?: readonly any[]) => Promise<T>; // eslint-disable-line @typescript-eslint/no-explicit-any
	readonly getRows: <T extends Row = Row>(input: QueryConfig | string, values?: readonly any[]) => Promise<readonly T[]>; // eslint-disable-line @typescript-eslint/no-explicit-any
	readonly getColumn: <T>(input: QueryConfig | string, values?: readonly any[], columnIndex?: number) => Promise<readonly T[]>; // eslint-disable-line @typescript-eslint/no-explicit-any
	readonly getOneColumn: <T>(input: QueryConfig | string, values?: readonly any[], columnIndex?: number) => Promise<T>; // eslint-disable-line @typescript-eslint/no-explicit-any
	readonly insertStream: <T extends Row = Row>(tableName: string, options?: InsertCollectorOptions) => Promise<DatabaseInsertStream<T>>;
	readonly deleteStream: <T extends OneDatabaseValue = string>(tableName: string, options?: DeleteCollectorOptions) => Promise<DatabaseDeleteStream<T>>;
}

export interface Connection extends AsyncConnection {
	readonly transaction: <T>(callback: (connection: Connection) => Promise<T> | T) => Promise<T>;

	readonly streamQuery: (input: QueryConfig | string, values?: readonly any[]) => Promise<DatabaseReadStream>; // eslint-disable-line @typescript-eslint/no-explicit-any
}
