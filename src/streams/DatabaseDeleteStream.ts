import {Writable} from 'stream';

import BatchDeleteCollector, {OneDatabaseValue} from '../collectors/BatchDeleteCollector';

export default class DatabaseDeleteStream<T extends OneDatabaseValue> extends Writable {
	private readonly batchDeleteCollector: BatchDeleteCollector<T>;

	public constructor(batchDeleteCollector: BatchDeleteCollector<T>) {
		super({
			objectMode: true,
			highWaterMark: batchDeleteCollector.getBatchSize(),
		});
		this.batchDeleteCollector = batchDeleteCollector;
	}

	// eslint-disable-next-line @typescript-eslint/naming-convention
	public _write(key: T, encoding: string, callback: (error?: Error) => void): void {
		try {
			this.batchDeleteCollector.add(key);
			callback();
		} catch (error: unknown) {
			callback(error instanceof Error ? error : new Error(`Delete stream error: ${String(error)}`));
		}
	}

	// eslint-disable-next-line @typescript-eslint/naming-convention
	public async _final(callback: (error?: Error) => void): Promise<void> {
		try {
			await this.batchDeleteCollector.flush();
			this.emit('deleting_finished', {
				affectedRowCount: this.batchDeleteCollector.getAffectedRowCount(),
			});
			callback();
		} catch (error: unknown) {
			callback(error instanceof Error ? error : new Error(`Delete stream error: ${String(error)}`));
		}
	}
}
