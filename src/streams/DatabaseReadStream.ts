import QueryStream from 'pg-query-stream';

export default class DatabaseReadStream extends QueryStream {
	// eslint-disable-next-line @typescript-eslint/naming-convention
	public _destroy(error: Error, callback: (callbackError?: Error | null) => void): void {
		// eslint-disable-next-line no-underscore-dangle
		super._destroy(error, (callbackError?: Error | null): void => {
			this.emit('close');
			callback(callbackError);
		});
	}
}
