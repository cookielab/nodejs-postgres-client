// original https://github.com/langpavel/node-pg-async/blob/master/src/sql.js

// eslint-disable-next-line filenames/match-exported
import {inspect} from 'util';
import pg, {QueryConfig} from 'pg';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type TransformType = (value: any) => SqlFragment;

const transforms: Record<string, TransformType> = {};

// debug purpose
const getTransformsOrNull = (name: string): TransformType | null => {
	return transforms[name];
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const registerTransform = (name: string, transform: (value: any) => SqlFragment): void => {
	const existing = Object.keys(transforms);
	if (existing.includes(name)) {
		throw new Error(`Transform ${name} already registered`);
	}
	transforms[name] = transform;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const removeTransform = (name: string): void => {
	// eslint-disable-next-line @typescript-eslint/no-dynamic-delete
	delete transforms[name];
};

// eslint-disable-next-line @typescript-eslint/no-use-before-define
const transform = (name: string, value: unknown): SqlFragment => {
	if (!transforms.hasOwnProperty(name)) {
		throw new Error(`Unknown transform: "${name}"`);
	}

	return transforms[name](value);
};

class SqlFragment implements QueryConfig {
	public values: unknown[] = [];
	public text: string;

	private readonly parts: string[] = [];
	private asString?: string;

	// SQL`insert into $quote${tableName} $values${keyValue}`;
	public constructor(templateParts: readonly string[], templateValues: unknown[]) {
		const length = templateValues.length;
		const text: string[] = [];
		let currentFragment: string[] = [];
		let argIndex = 1;

		let i = 0;

		const addText = (str: string): void => {
			currentFragment.push(str);
		};

		const flushText = (): void => {
			const fragment = currentFragment.join('');
			currentFragment = [];
			this.parts.push(fragment);
			text.push(fragment);
		};

		const addValue = (value: unknown): void => {
			flushText();
			this.values.push(value);
			text.push('$', String(argIndex++));
		};

		while (i < length) {
			const parts = templateParts[i].split('$');
			let value = templateValues.shift();
			if (typeof value === 'undefined') {
				throw new Error(`Expected something, but got undefined. `
					+ `Value after SQL fragment: ${text.join('')}${templateParts[i]}`);
			}

			while (parts.length > 1) {
				const name = parts.pop();
				if (name != null) {
					value = transform(name, value);
				}
			}

			addText(parts[0]);

			if (value instanceof SqlFragment) {
				const nestedValuesLength = value.values.length;
				let valueIndex = 0;
				while (valueIndex < nestedValuesLength) {
					addText(value.parts[valueIndex]);
					addValue(value.values[valueIndex]);
					valueIndex++;
				}
				addText(value.parts[valueIndex]);
			} else {
				addValue(value);
			}
			i++;
		}
		// last part is alone, without value
		addText(templateParts[i]);
		flushText();

		this.text = text.join('');
	}

	// this is for log/debugging only
	public toString(): string {
		if (this.asString != null) {
			return this.asString;
		}

		const text = [];
		const length = this.values.length;
		let i = 0;
		while (i < length) {
			text.push(this.parts[i]);
			const value = transform('literal', this.values[i]);
			text.push(value);
			i++;
		}
		text.push(this.parts[i]);
		this.asString = text.join('');

		return this.asString;
	}
}

// eslint-disable-next-line @typescript-eslint/naming-convention
const SQL = (parts: SqlFragment | TemplateStringsArray | string, ...values: unknown[]): SqlFragment => {
	if (parts instanceof SqlFragment) {
		return parts;
	}

	if (typeof parts === 'string') {
		return new SqlFragment([parts], values);
	}

	if (!Array.isArray(parts) && values.length !== 0) {
		throw new Error(`Calling SQL manually with values`);
	}

	return new SqlFragment(parts, values);
};

SQL.NULL = SQL('NULL');
SQL.DEFAULT = SQL('DEFAULT');

// eslint-disable-next-line @typescript-eslint/unbound-method,import/no-named-as-default-member
const escapeIdentifier = pg.Client.prototype.escapeIdentifier;
// eslint-disable-next-line @typescript-eslint/unbound-method,import/no-named-as-default-member
const escapeLiteral = pg.Client.prototype.escapeLiteral;

const sqlStr = (str: unknown): SqlFragment => {
	if (typeof str !== 'string') {
		throw new Error(`Expected string, got ${inspect(str)}`);
	}

	return SQL(str);
};

// returns quoted identifier
const identifier = (name: unknown): SqlFragment => {
	if (typeof name !== 'string' || name === '') {
		throw new Error(`Expected nonempty string, got ${inspect(name)}`);
	}

	return SQL(escapeIdentifier(name));
};

// returns quoted literal
const literal = (value: unknown): SqlFragment => {
	if (value instanceof SqlFragment) {
		return value;
	}

	if (typeof value === 'undefined') {
		throw new Error(`Expected something, but got undefined.`);
	}

	if (value === null) {
		return SQL.NULL;
	}

	return SQL(escapeLiteral(String(value)));
};

const isObject = (value: unknown): value is Readonly<Record<string, unknown>> => {
	return typeof value === 'object'
		&& value != null
		&& !Array.isArray(value);
};

const insertObject = (data: unknown): SqlFragment => {
	if (!isObject(data)) {
		throw new Error(`Expected record`);
	}

	const keys = Object.keys(data);
	const length = keys.length;
	const sqlFragments = new Array(length);
	const values: unknown[] = new Array(length - 1);
	const sb = [];

	sb.push('(');
	let i = 0;
	while (i < length) {
		const column = keys[i];
		values[i] = data[column];
		i++;
		sb.push(escapeIdentifier(column), ',');
		sqlFragments[i] = ',';
	}
	sb[sb.length - 1] = ') VALUES (';
	sqlFragments[0] = sb.join('');
	sqlFragments[i] = ')';

	return new SqlFragment(sqlFragments, values);
};

registerTransform('id', identifier);
registerTransform('ident', identifier);
registerTransform('identifier', identifier);
registerTransform('name', identifier);
registerTransform('literal', literal);
registerTransform('raw', sqlStr);
registerTransform('!', sqlStr);
registerTransform('insert', insertObject);
registerTransform('insert_object', insertObject);

export {
	SQL,
	SqlFragment,
	removeTransform,
	registerTransform,
	getTransformsOrNull,
};
