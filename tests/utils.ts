const sleep = async (ms: number): Promise<void> => {
	await new Promise<void>((resolve: () => void): void => {
		setTimeout(resolve, ms);
	});
};

export {sleep};
