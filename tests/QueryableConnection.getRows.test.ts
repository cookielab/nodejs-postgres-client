import {Row} from '../src';
import Client from '../src/Client';
import {createFakePool} from './bootstrap';

describe('QueryableConnection.getRows', () => {
	it('returns only the result rows', async () => {
		const resultRows: Row[] = [];
		const connection = new Client(createFakePool());
		jest.spyOn(connection, 'query')
			.mockImplementation(() => Promise.resolve({
				command: 'INSERT',
				rowCount: 0,
				oid: 0,
				fields: [],
				rows: resultRows,
			}));

		const result = await connection.getRows('');
		expect(result).toBe(resultRows);
	});
});
