import columnNameTransformer from '../../src/transformers/columnNameTransformer';
import columnNamesTransformer from '../../src/transformers/columnNamesTransformer';
import insertTransformer from '../../src/transformers/insertTransformer';
import multiInsertTransformer from '../../src/transformers/multiInsertTransformer';
import valueListTransformer from '../../src/transformers/valueListTransformer';
import valuesTableTransformer from '../../src/transformers/valuesTableTransformer';
import {registerTransform} from '../../src/SQL';

registerTransform('columnName', columnNameTransformer);
registerTransform('columnNames', columnNamesTransformer);
registerTransform('values', valueListTransformer);
registerTransform('valuesTable', valuesTableTransformer);
registerTransform('multiInsert', multiInsertTransformer);

describe('insert transformer', () => {
	it('prepares insert for one row', () => {
		const sql = insertTransformer({id: 'id1', name: 'name1', integer: 1});

		expect(sql.text.trim()).toBe('("id", "name", "integer") VALUES ($1, $2, $3)');
		expect(sql.values).toEqual([
			'id1', 'name1', 1,
		]);
	});

	it('fails for empty object', () => {
		expect(() => {
			insertTransformer({});
		}).toThrow(new Error('Cannot format insert for rows of empty objects.'));
	});
});
