import {Pool} from 'pg';

import Client from '../src/Client';

describe('Client.end', () => {
	it('recalls end on the original pool', async () => {
		const pool: Pool = new (jest.fn())();
		pool.end = jest.fn(() => Promise.resolve());

		const client = new Client(pool);

		await client.end();

		expect(pool.end).toHaveBeenCalledTimes(1);
	});
});
